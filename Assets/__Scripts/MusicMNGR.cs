﻿using UnityEngine;
using System.Collections;

public class MusicMNGR : MonoBehaviour
{
	public AudioClip[] Randoms;
	public AudioSource IntMenuSource;
	public AudioSource RandomSource;
	public AudioSource StorySource;
	AudioSource audio;
	float Progress;

	void Awake()
	{
		IntMenuSource.Play();
		RandomSource.Play();
		StorySource.Play();

		IntMenuSource.Pause();
		RandomSource.Pause();
		StorySource.Pause();

		MusicMNGR[] MNGRs = FindObjectsOfType<MusicMNGR>();
		foreach(MusicMNGR MNGR in MNGRs)
		{
			if(MNGR.gameObject.GetInstanceID() != gameObject.GetInstanceID())
			{
				Destroy(gameObject);
			}
		}
		DontDestroyOnLoad(gameObject);

		audio = GetComponent<AudioSource>();
	}
	void Update()
	{
		if(!audio.isPlaying)
		{
			audio.Play();
		}

		IntMenuSource.Pause();
		RandomSource.Pause();
		StorySource.Pause();

		if(Application.loadedLevel == 1)
		{
			Progress = 0f;
			IntMenuSource.UnPause();
		}
		else if(Application.loadedLevel == 2 || Application.loadedLevel == 4 || Application.loadedLevel == 0)
		{
			RandomSource.UnPause();

			if(RandomSource.time < Progress)
			{
				RandomSource.clip = Randoms[Random.Range(0, Randoms.Length - 1)];
			}
			Progress = RandomSource.time;
		}
		else if(Application.loadedLevel == 3)
		{
			Progress = 0f;
			StorySource.UnPause();
		}
	}
}