﻿using UnityEngine;
using System.Collections;

public class RandomTopGenerator : MonoBehaviour
{
	public float YFrom = 10f;
	public float YTo = 12f;
	public GameObject[] ToGen;
	public bool RandomSeed = true;
	public int Seed;

	void Awake()
	{
		if(RandomSeed)
		{
			Seed = Random.Range(0, 100000);
		}
		Random.seed = Seed;
	}
	public void LegCalled(GameObject TopPrefab, float DistanceDealt, float BarrierEvery)
	{
		GameObject TopInstance = Instantiate(TopPrefab);
		
		TopInstance.transform.position = new Vector3(DistanceDealt + (2.2f * BarrierEvery), TopPrefab.transform.position.y, TopPrefab.transform.position.z);
		
		TopInstance.transform.parent = transform;
	}
	public void Called(float DistanceDealt, float BarrierEvery)
	{
		GameObject CurrentPrefab = ToGen[Random.Range(0, ToGen.Length)];

		GameObject TopInstance = Instantiate(CurrentPrefab);
		
		TopInstance.transform.position = new Vector3(DistanceDealt + (2.2f * BarrierEvery), Random.Range(YFrom, YTo), CurrentPrefab.transform.position.z);
		
		TopInstance.transform.parent = transform;
	}
}