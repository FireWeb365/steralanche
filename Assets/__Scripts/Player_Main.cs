﻿using UnityEngine;
using System.Collections;

public class Player_Main : MonoBehaviour
{
	public bool CanDie = true;

	public void Die()
	{
		if(CanDie)
		{
			FindObjectOfType<GameCotrol>().Lost = true;

			Time.timeScale = 0.1f;
		}
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.collider.tag == "Killer")
		{
			Die();
		}
	}
}