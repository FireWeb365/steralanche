﻿using UnityEngine;
using System.Collections;

public class Player_Shooter : MonoBehaviour
{
	public int MouseBTN = 0;
	public float ProjSpeed = 3f;
	public GameObject ProjectilePrefab;
	GameCotrol GC;
	
	void Awake()
	{
		GC = FindObjectOfType<GameCotrol>();
	}
	void Update()
	{
		if(Input.GetMouseButtonDown(MouseBTN) && !GC.Lost && !GC.TimeStopped)
		{
			Vector2 Normal = new Vector2(-(transform.position.x - Camera.main.ScreenToWorldPoint(Input.mousePosition).x), -(transform.position.y - Camera.main.ScreenToWorldPoint(Input.mousePosition).y));
			Normal.Normalize();
			
			RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Normal, 16f);

			GameObject PInstance = Instantiate(ProjectilePrefab, transform.position, Quaternion.identity) as GameObject;

			Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - PInstance.transform.position;
			diff.Normalize();

			float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

			PInstance.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, rot_z));

			PInstance.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * ProjSpeed);
		}
	}
}