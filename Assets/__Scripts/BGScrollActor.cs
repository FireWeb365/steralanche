﻿using UnityEngine;
using System.Collections;

public class BGScrollActor : MonoBehaviour
{
	public float FragmentSpeed = 0.4f;
	ScrollControl Scroll;

	void Awake()
	{
		Scroll = FindObjectOfType<ScrollControl>();
	}
	void Update()
	{
		transform.position += Vector3.right * Time.deltaTime * Scroll.Speed * FragmentSpeed;
	}
}