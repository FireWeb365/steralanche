﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	public bool DestroyOnCol = true;

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.collider.tag != "Ghost")
		{
			if(DestroyOnCol)
			{
				Destroy(gameObject);

				if(col.gameObject.tag == "QuestKillable")
				{
					if(FindObjectOfType<Story_Core>())
					{
						FindObjectOfType<Story_Core>().CurEnemies++;
					}
				}
			}
		}
	}
}