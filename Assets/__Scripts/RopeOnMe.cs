﻿using UnityEngine;
using System.Collections;

public class RopeOnMe : MonoBehaviour
{
	LineRenderer line;

	void Awake()
	{
		line = GetComponent<LineRenderer>();
	}
	void Update()
	{
		line.SetPosition(1, transform.position);
	}
}