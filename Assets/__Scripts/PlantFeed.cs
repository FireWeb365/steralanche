﻿using UnityEngine;
using System.Collections;

public class PlantFeed : MonoBehaviour
{
	void OnMouseDown()
	{
		transform.localScale = transform.localScale + new Vector3(0.1f, 0.1f, 0.1f);

		if(transform.localScale.x > 6f)
		{
			transform.localScale = Vector3.one;
		}
	}
}