﻿using UnityEngine;
using System.Collections;

public class MouseResponse : MonoBehaviour
{
	public GameObject Right;
	public GameObject Middle;
	public GameObject Left;

	void Update()
	{
		if(Input.GetMouseButton(0))
		{
			Left.SetActive(true);
		}
		else
		{
			Left.SetActive(false);
		}
		if(Input.GetMouseButton(1))
		{
			Right.SetActive(true);
		}
		else
		{
			Right.SetActive(false);
		}
		if(Input.GetMouseButton(2))
		{
			Middle.SetActive(true);
		}
		else
		{
			Middle.SetActive(false);
		}
	}
}