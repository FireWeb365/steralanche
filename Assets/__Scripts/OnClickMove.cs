﻿using UnityEngine;
using System.Collections;

public class OnClickMove : MonoBehaviour
{
	public float PosSpeed = 9f;
	public float SizeSpeed = 3f;
	public int FinalScene;
	public Vector3[] PoSs;
	public float[] Sizes;
	int Cur;

	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Cur++;
			if(Cur + 1 > PoSs.Length)
			{
				Application.LoadLevel(FinalScene);
			}
		}
		else if(Input.GetMouseButtonDown(1))
		{
			Cur--;
			if(Cur < 0)
			{
				Cur = 0;
			}
		}

		transform.position = Vector3.Slerp(transform.position, PoSs[Cur], Time.deltaTime * PosSpeed);
		Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, Sizes[Cur], Time.deltaTime * SizeSpeed);
	}
}