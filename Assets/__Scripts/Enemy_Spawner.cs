﻿using UnityEngine;
using System.Collections;

public class Enemy_Spawner : MonoBehaviour
{
	public GameObject[] Enemies;
	public float DelayFrom = 3f;
	public float DelayTo = 8f;
	public float YPosRand = 4f;
	float DelayLeft;

	void Update()
	{
		if(Enemies.Length > 0)
		{
			DelayLeft -= Time.deltaTime;

			if(DelayLeft < 0f)
			{
				DelayLeft = Random.Range(DelayFrom, DelayTo);
				
				Vector3 InstPos = transform.position + new Vector3(0f, Random.Range(-YPosRand, YPosRand), 0f);

				InstPos.z = 0f;

				GameObject Instance = Instantiate(Enemies[Random.Range(0, Enemies.Length - 1)], InstPos, Quaternion.identity) as GameObject;
			}
		}
	}
}