﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Story_Provider : MonoBehaviour
{
	public List<Quest> CurrentQuests;
	public int CurrentQuestNum;
	public int ID = -1;
	public Quest My;
	public Text QuestTitle;
	public Text QuestDescription;
	float UpdateAfter = 0.5f;

	void Update()
	{
		UpdateAfter -= Time.deltaTime;

		if(UpdateAfter <= 0f)
		{
			UpdateAfter = 1000f;

			Starter();
		}
	}
	void Starter()
	{
		if(My.ID == -1)
		{
			My = CurrentQuests[CurrentQuestNum];
		}

		UpdateUI();
	}
	public void UpdateUI()
	{
		QuestTitle.text = My.Name;

		QuestDescription.text = My.Description;
	}
	public void CompleteQuest()
	{
		My = CurrentQuests[CurrentQuestNum];

		UpdateUI();
	}
	public void PlayQuest()
	{
		FindObjectOfType<Story_Core>().CurrentQuest = My;

		FindObjectOfType<Story_Core>().SaveProgressNLoad(3);
	}
}