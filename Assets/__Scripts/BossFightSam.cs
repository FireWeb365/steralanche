﻿using UnityEngine;
using System.Collections;

public class BossFightSam : MonoBehaviour
{
	public GameObject ToActivate;
	Story_Core core;

	void Awake()
	{
		core = FindObjectOfType<Story_Core>();
	}
	void Update()
	{
		if(core.Progress >= 100)
		{
			ToActivate.SetActive(true);
		}
	}
	public void PlayBoss()
	{
		Application.LoadLevel(5);
	}
}