﻿using UnityEngine;
using System.Collections;

public class MenuFuncs : MonoBehaviour
{
	public bool AutoResetTime;
	float ResumeTimeSc = 1f;



	void Awake()
	{
		if(AutoResetTime)
		{
			Time.timeScale = 1f;
		}
	}
	public void Menu()
	{
		Application.LoadLevel(0);
	}
	public void SetUsername(string NewOne)
	{
		PlayerPrefs.SetString("PName", NewOne);
	}
	public void StopTime()
	{
		if(!FindObjectOfType<GameCotrol>().TimeStopped)
		{
			FindObjectOfType<GameCotrol>().TimeStopped = true;

			ResumeTimeSc = Time.timeScale;

			Time.timeScale = 0f;
		}
	}
	public void ResumeTime()
	{
		if(FindObjectOfType<GameCotrol>().TimeStopped)
		{
			FindObjectOfType<GameCotrol>().TimeStopped = false;

			Time.timeScale = ResumeTimeSc;
		}
	}
	public void Play()
	{
		Application.LoadLevel(1);
	}
	public void Play(int ID)
	{
		Application.LoadLevel(ID);
	}
	public void VisitSite()
	{
		Application.OpenURL("http://www.fireweb.eu/games/realeases/steralanche/");
	}
	public void ArtSite()
	{
		Application.OpenURL("http://google.sk/");
	}
	public void Exit()
	{
		Application.Quit();
	}
	public void ActivateObject(GameObject GObject)
	{
		GObject.SetActive(true);
	}
	public void DeActivateObject(GameObject GObject)
	{
		GObject.SetActive(false);
	}
}