using UnityEngine;
using System.Collections;

public class Player_Rope : MonoBehaviour
{
	public Vector3 TargetPos;
	public float LerpSpeed = 10f;
	public bool Visible = false;
	public LineRenderer Rope;
	[HideInInspector]
	public bool Reset;
	Vector3 LastOnePos;

	void Update()
	{
		if(Reset)
		{
			LastOnePos = transform.position;
			Rope.SetPosition(1, LastOnePos);

			Reset = false;
		}

		Rope.enabled = Visible;

		Rope.SetPosition(0, transform.position);

		LastOnePos = Vector3.Lerp(LastOnePos, TargetPos, Time.deltaTime * LerpSpeed);
		Rope.SetPosition(1, LastOnePos);
	}
}