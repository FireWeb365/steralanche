﻿using UnityEngine;
using System.Collections;

public class DestroyerAffected : MonoBehaviour
{
	//static float Below = 60f;

	void Update()
	{
		if(Vector3.Distance(transform.position, Camera.main.transform.position) >= 80f)
		{
			Destroy(gameObject);
		}
	}
}