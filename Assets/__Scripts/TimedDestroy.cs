﻿using UnityEngine;
using System.Collections;

public class TimedDestroy : MonoBehaviour
{
	public float Time = 3f;

	void Start()
	{
		Destroy(gameObject, Time);
	}
}