﻿using UnityEngine;
using System.Collections;

public class DynamicBG : MonoBehaviour
{
	//PixelChange
	public bool PixelChange = true;
	public float PercentPerSec = 14f;
	public Sprite To;
	float TLeft = 0f;
	float Left;
	SpriteRenderer renderer;
	Sprite TSet;
	Texture2D TTo;

	void Awake()
	{
		renderer = GetComponent<SpriteRenderer>();

		TLeft = ((To.texture.width * To.texture.height) / 100) * PercentPerSec; //How many times a sec will change pixel color

		TLeft = 1 / TLeft;

		TSet = renderer.sprite;

		TTo = To.texture;
	}
	void Update()
	{
		if(PixelChange)
		{
			Left -= Time.deltaTime;

			if(Left < 0f)
			{
				int He = Random.Range(0, To.texture.height);
				int Wi = Random.Range(0, To.texture.width);

				TSet.texture.SetPixel(Wi, He, TTo.GetPixel(Wi, He));

				renderer.sprite = TSet;

				Left = TLeft;
			}
		}
	}
}