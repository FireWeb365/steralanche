﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scroller : MonoBehaviour
{
	public Text DistTXT;
	public Text BDistTXT;
	public Text RankTXT;
	public Camera Cam;
	[HideInInspector]
	public float Speed = 2f;
	public float DistanceDealt = 0f;
	[HideInInspector]
	public float ActualScroll;
	string StartText;
	string StartText2;
	string StartText3;
	GameCotrol GC;
	float CurrentBest;
	HighScore HS;

	void Awake()
	{
		if(DistTXT != null)
		{
			StartText = DistTXT.text;
		}
		if(BDistTXT != null)
		{
			StartText2 = BDistTXT.text;
		}
		if(RankTXT != null)
		{
			StartText3 = RankTXT.text;
		}

		GC = FindObjectOfType<GameCotrol>();
		HS = FindObjectOfType<HighScore>();

		if(BDistTXT != null)
		{
			if(!GC.Lost)
			{
				BDistTXT.text = StartText2 + Mathf.Round(PlayerPrefs.GetFloat("BestInRand", 0f));
			}
		}
		CurrentBest = PlayerPrefs.GetFloat("BestInRand", 0f);
	}
	void LateUpdate()
	{
		ActualScroll += Speed * Time.deltaTime;
		
		DistanceDealt += ActualScroll;

		Cam.transform.position = Cam.transform.position + new Vector3(ActualScroll, 0f, 0f);

		if(DistTXT != null)
		{
			if(!GC.Lost)
			{
				DistTXT.text = StartText + Mathf.Round(DistanceDealt);
			}
		}
		if(DistanceDealt > CurrentBest && !GC.Lost)
		{
			CurrentBest = DistanceDealt;

			PlayerPrefs.SetFloat("BestInRand", DistanceDealt);

			UpdateBest();
		}
		if(RankTXT != null)
		{
			RankTXT.text = StartText3 + HS.Rank;
		}
	}
	public void UpdateBest()
	{
		if(BDistTXT != null)
		{
			if(!GC.Lost)
			{
				BDistTXT.text = StartText2 + Mathf.Round(PlayerPrefs.GetFloat("BestInRand", 0f));
			}
		}

		if(HS != null)
		{
			HS.GrabRank();
		}
	}
}