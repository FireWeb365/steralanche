﻿using UnityEngine;
using System.Collections;

public class onEnableResend : MonoBehaviour
{
	public GameObject Dest;
	
	void OnEnable()
	{
		if(Dest != null)
		{
			Dest.SendMessage("OnEnable");
		}
	}
}