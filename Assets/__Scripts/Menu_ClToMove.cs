﻿using UnityEngine;
using System.Collections;

public class Menu_ClToMove : MonoBehaviour
{
	public int MouseBTN = 0;
	public GameObject AffectPos;
	public LayerMask RayLM;
	NavMeshAgent agent;
	Vector3 Dest;

	void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
	}
	void Update()
	{
		if(AffectPos != null)
		{
			AffectPos.transform.position = transform.position;
		}

		if(Input.GetMouseButtonDown(MouseBTN))
		{
			Dest = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			RaycastHit hit;

			if(Physics.Raycast(ray, out hit))
			{
				Dest = hit.point;
			}

			agent.SetDestination(Dest);
		}
	}
	void Move()
	{

	}
}