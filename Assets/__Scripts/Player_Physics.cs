﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (DistanceJoint2D))]
public class Player_Physics : MonoBehaviour
{
	[HideInInspector]
	public DistanceJoint2D Joint;

	void Awake()
	{
		Joint = GetComponent<DistanceJoint2D>();
	}
}