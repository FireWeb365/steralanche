﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Story_ProgressBar : MonoBehaviour
{
	[HideInInspector]
	public bool StoryEnded;
	Image img;
	Text txt;
	Story_Core Core;

	void Awake()
	{
		img = GetComponent<Image>();
		txt = GetComponentInChildren<Text>();

		Core = FindObjectOfType<Story_Core>();
	}
	void Update()
	{
		img.fillAmount = Core.Progress / 100f;
		
		txt.text = Core.Progress + "%";
	}
}