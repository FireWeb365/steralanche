﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderToPref : MonoBehaviour
{
	public string PrefName = "Slider";
	Slider slider;

	void Awake()
	{
		slider = GetComponent<Slider>();

		slider.value = PlayerPrefs.GetFloat(PrefName, slider.value);
	}
	public void Update()
	{
		PlayerPrefs.SetFloat(PrefName, slider.value);
	}
}