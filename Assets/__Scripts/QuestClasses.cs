﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Quest
{
	public int ID = -1;
	public int Progress = 0;
	public string Name = "First quest";
	public string Description = "Example quest";
	public QuestType QuestType;
	public float IfDistamce;
	public int IfEnemies;
	public LevelSettings LevelSettings;
	public bool Reserved;
	public bool Done;
	public int ProviderID;
}
[System.Serializable]
public enum QuestType
{
	Lvl_Distance,
	Lvl_Enemies
}
[System.Serializable]
public class LevelSettings
{
	public GameObject[] Enemies;
	public GameObject Top;
	public GameObject Bot;
	public float DelayFrom = 3f;
	public float DelayTo = 8f;
}