﻿using UnityEngine;
using System.Collections;

public class IfEnabledPlay : MonoBehaviour
{
	bool Already;

	void Update()
	{
		if(!Already)
		{
			if(GetComponent<AudioSource>().enabled)
			{
				GetComponent<AudioSource>().Play();
			}
		}
	}
}