﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class HighScore : MonoBehaviour
{
	public Text[] ScoreTexts;

	private string privateKey = "ba^y*g~SPB5_kPaYF-~~a37vTB8v58xw0LSzfOJEu 6rOUw^Fj87wvvBNgyP";
	private string TopScoresURL = "http://spongia.fireweb.eu/TopScores.php";
	private string AddScoreURL = "http://spongia.fireweb.eu/AddScore.php?";
	private string RankURL = "http://spongia.fireweb.eu/GetRank.php?";
	
	public int Rank;

	public string[] textlist;
	public string[] Names;
	public string[] Scores;

	void OnEnable()
	{
		if(PlayerPrefs.GetString("PName", "anonymouz") == "")
		{
			PlayerPrefs.SetString("PName", "anonymouz");
		}
		string PName = PlayerPrefs.GetString("PName", "anonymouz");
		int BestInRand = Mathf.RoundToInt(PlayerPrefs.GetFloat("BestInRand", 0f));

		StartCoroutine(AddScore(PName, BestInRand));

		StartCoroutine(GetTopScores());
		
		StartCoroutine(GrabRank(PName));
	}
	private string Md5Sum(string strToEncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);
		
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);
		
		string hashString = "";
		
		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}
		
		return hashString.PadLeft(32, '0');
	}
	IEnumerator AddScore(string name, int score)
	{
		string hash = Md5Sum(name + score + privateKey);
		
		WWW ScorePost = new WWW(AddScoreURL + "name=" + name + "&score=" + score + "&hash=" + hash);
		yield return ScorePost;
		
		if (ScorePost.error == null)
		{
			print("Added score apaz");

			StartCoroutine(GrabRank(name));
		}
		else
		{
			Debug.Log(ScorePost.error);
		}
	}
	IEnumerator GrabRank(string name)
	{
		WWW RankGrabAttempt = new WWW(RankURL + "name=" + WWW.EscapeURL(name));
		
		yield return RankGrabAttempt;

		if(RankGrabAttempt.error == null)
		{
			Rank = System.Int32.Parse(RankGrabAttempt.text);
		}
	}
	public void GrabRank()
	{
		string PName = PlayerPrefs.GetString("PName", "anonymouz");
		
		StartCoroutine(GrabRank(PName));
	}
	IEnumerator GetTopScores()
	{
		WWW GetScoresAttempt = new WWW(TopScoresURL);
		yield return GetScoresAttempt;
		
		if (GetScoresAttempt.error == null)
		{
			print("Top scores apaz");

			textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);

			Names = new string[Mathf.FloorToInt(textlist.Length/2)];
			Scores = new string[Names.Length];
			for (int i = 0; i < textlist.Length; i++)
			{
				if (i % 2 == 0)
				{
					if(Names.Length >= Mathf.FloorToInt(i / 2))
					{
						Debug.Log(Mathf.FloorToInt(i / 2).ToString());
						Names[Mathf.FloorToInt(i / 2)] = textlist[i];
					}
				}
				else Scores[Mathf.FloorToInt(i / 2)] = textlist[i];
			}

			int O = 0;
			foreach(Text ScoreText in ScoreTexts)
			{
				if(Names.Length >= O && Scores.Length >= O)
				{
					ScoreText.text = Names[O] + " - " + Scores[O];

					O++;
				}
			}
		}
	}
}