﻿using UnityEngine;
using System.Collections;

public class GameCotrol : MonoBehaviour
{
	public bool Lost;
	public GameObject LoseScreen;
	public bool TimeStopped;

	void Update()
	{
		LoseScreen.SetActive(Lost);

		if(Lost)
		{
			GameObject.FindWithTag("Player").GetComponent<Collider2D>().enabled = false;

			GameObject.FindWithTag("Player").GetComponent<DistanceJoint2D>().enabled = false;
		}
	}
	public void Restart()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
	public void Menu()
	{
		Application.LoadLevel(0);
	}
	public void IntMenu()
	{
		Application.LoadLevel(1);
	}
}