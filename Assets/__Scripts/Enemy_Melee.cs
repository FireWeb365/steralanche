﻿using UnityEngine;
using System.Collections;

public class Enemy_Melee : MonoBehaviour
{
	public GameObject DeathObject;
	public bool Kamikadze;
	public bool BulletProof;

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.collider.tag == "Bullet")
		{
			if(!BulletProof)
			{
				GetDestroyed();
			}
		}
		else if(col.collider.tag == "Player")
		{
			if(Kamikadze)
			{
				col.collider.GetComponent<Player_Main>().Die();

				GetDestroyed();
			}
		}
	}
	public void GetDestroyed()
	{
		if(DeathObject != null)
		{
			Instantiate(DeathObject, transform.position, transform.rotation);
		}

		Destroy(gameObject);
	}
}