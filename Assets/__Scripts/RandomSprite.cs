﻿using UnityEngine;
using System.Collections;

public class RandomSprite : MonoBehaviour
{
	public Sprite[] Sprites;

	void Awake()
	{
		GetComponent<SpriteRenderer>().sprite = Sprites[Random.Range(0, Sprites.Length)];
	}
}