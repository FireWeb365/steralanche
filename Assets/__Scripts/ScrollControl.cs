﻿using UnityEngine;
using System.Collections;

public class ScrollControl : MonoBehaviour
{
	public Camera Cam;
	public float Speed = 2f;
	public GameObject BGPrefab;
	public GameObject TopPrefab;
	public GameObject BotPrefab;
	public float BarrierEvery = 20f;
	public float BGEvery = 20f;
	public bool UseTopGen;
	float LeftToNewBarriers;
	float LeftToNewBG;
	RandomTopGenerator RTG;
	Scroller sc;

	void Start()
	{
		sc = FindObjectOfType<Scroller>();
		sc.Speed = Speed;
		RTG = FindObjectOfType<RandomTopGenerator>();
		if(BGPrefab != null)
		{
			GameObject BGInstance;

			BGInstance = Instantiate(BGPrefab);
			BGInstance.transform.position = new Vector3(sc.DistanceDealt + (1f * BGEvery), BGPrefab.transform.position.y, BGPrefab.transform.position.z);

			BGInstance = Instantiate(BGPrefab);
			BGInstance.transform.position = new Vector3(sc.DistanceDealt + (0f * BGEvery), BGPrefab.transform.position.y, BGPrefab.transform.position.z);

			BGInstance = Instantiate(BGPrefab);
			BGInstance.transform.position = new Vector3(sc.DistanceDealt + (2f * BGEvery), BGPrefab.transform.position.y, BGPrefab.transform.position.z);
			
			LeftToNewBG = BGEvery;
		}
	}
	void Update()
	{
		if(BarrierEvery != 0)
		{
			LeftToNewBarriers -= sc.ActualScroll;
			
			if(LeftToNewBarriers < 0f)
			{
				if(UseTopGen)
				{
					RTG.Called(sc.DistanceDealt, BarrierEvery);
				}
				else
				{
					GameObject TopInstance = Instantiate(TopPrefab);
					
					TopInstance.transform.position = new Vector3(sc.DistanceDealt + (2.2f * BarrierEvery), TopPrefab.transform.position.y, TopPrefab.transform.position.z);
					
					TopInstance.transform.parent = transform;
				}
				GameObject BotInstance = Instantiate(BotPrefab);

				BotInstance.transform.position = new Vector3(sc.DistanceDealt + (2.2f * BarrierEvery), BotPrefab.transform.position.y, BotPrefab.transform.position.z);

				BotInstance.transform.parent = transform;
				
				LeftToNewBarriers = BarrierEvery;
			}
		}
		if(BGEvery != 0)
		{
			LeftToNewBG -= sc.ActualScroll;

			if(LeftToNewBG < 0f)
			{
				GameObject BGInstance = Instantiate(BGPrefab);

				BGInstance.transform.position = new Vector3(sc.DistanceDealt + (2f * BGEvery), BGPrefab.transform.position.y, BGPrefab.transform.position.z);

				LeftToNewBG = BGEvery;
			}
		}
		sc.ActualScroll = 0f;
	}
}