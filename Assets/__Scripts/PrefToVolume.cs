﻿using UnityEngine;
using System.Collections;

public class PrefToVolume : MonoBehaviour
{
	public string PrefName = "Slider";

	void Awake()
	{
		AudioSource[] Sources = GetComponents<AudioSource>();

		foreach(AudioSource Source in Sources)
		{
			Source.volume = PlayerPrefs.GetFloat(PrefName, GetComponent<AudioSource>().volume);
		}
	}
}