﻿using UnityEngine;
using System.Collections;

public class RandomGetHarder : MonoBehaviour
{
	public bool RandomRockets;
	public GameObject RPrefab;
	public float RForce = 1000f;
	public float SpawnFrom = 7f;
	public float SpawnTo = 13f;
	public float PosTreshold = 3f;
	public float TimeGetHarder = 0.1f;
	public float RTimeGetHarder = 0.1f;
	float TillNextR;
	Enemy_Spawner ES;

	void Awake()
	{
		TillNextR = Random.Range(SpawnFrom, SpawnTo);

		ES = FindObjectOfType<Enemy_Spawner>();
	}
	void Update()
	{
		if(ES.DelayFrom > 0.4f)
		{
			ES.DelayFrom -= Time.deltaTime * TimeGetHarder;
		}
		if(SpawnFrom > 4f)
		{
			SpawnFrom -= Time.deltaTime * RTimeGetHarder;
		}
		if(RandomRockets)
		{
			TillNextR -= Time.deltaTime;

			if(TillNextR <= 0f)
			{
				TillNextR = Random.Range(SpawnFrom, SpawnTo);

				GameObject Rocket = Instantiate(RPrefab, new Vector3(transform.position.x, transform.position.y + Random.Range(-PosTreshold, PosTreshold), transform.position.z), RPrefab.transform.rotation) as GameObject;
			
				if(Rocket.GetComponent<Rigidbody2D>())
				{
					Rocket.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * RForce * Rocket.GetComponent<Rigidbody2D>().mass);
				}
			}
		}
	}
}