﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Story_Core : MonoBehaviour
{
	public Quest CurrentQuest;
	public bool StayThru;
	public int Progress = 0;
	public bool PlayingQuest;
	public int CurEnemies;
	Scroller sc_dist;
	ScrollControl sc_prefabs;
	Enemy_Spawner sc_spawner;
	[HideInInspector]
	bool Longer;
	float ToSave;
	Image inlevelprogress;

	void Awake()
	{
		Story_Core[] Cores = FindObjectsOfType<Story_Core>();
		foreach(Story_Core Core in Cores)
		{
			if(Core.gameObject.GetInstanceID() != gameObject.GetInstanceID())
			{
				Destroy(gameObject);
			}
		}

		if(StayThru)
		{
			DontDestroyOnLoad(gameObject);
		}
		if(Application.loadedLevel == 1)
		{
			ToSave = 0.2f;
		}
	}
	void OnLevelWasLoaded(int level)
	{
		CurEnemies = 0;

		if(level == 3)
		{
			PlayingQuest = true;

			sc_dist = FindObjectOfType<Scroller>();
			sc_prefabs = FindObjectOfType<ScrollControl>();
			sc_spawner = FindObjectOfType<Enemy_Spawner>();

			//Level define
			sc_spawner.Enemies = CurrentQuest.LevelSettings.Enemies;
			
			sc_spawner.DelayFrom = CurrentQuest.LevelSettings.DelayFrom;
			sc_spawner.DelayTo = CurrentQuest.LevelSettings.DelayTo;
			//
		}
		else if(level == 1)
		{
			LoadProgress();

			PlayingQuest = false;
		}
		else
		{
			PlayingQuest = false;
		}
	}
	void Update()
	{
		if(PlayingQuest)
		{
			if(inlevelprogress == null)
			{
				inlevelprogress = GameObject.Find("inlevelprogress").GetComponent<Image>();
			}

			inlevelprogress.fillAmount = 0f;

			if(CurrentQuest.QuestType == QuestType.Lvl_Distance)
			{
				float A = CurrentQuest.IfDistamce;
				float B = sc_dist.DistanceDealt;

				inlevelprogress.fillAmount = (B / A) * 0.1f / 0.1f;
			}
			else if(CurrentQuest.QuestType == QuestType.Lvl_Enemies)
			{
				float A = CurrentQuest.IfEnemies;
				float B = CurEnemies;

				inlevelprogress.fillAmount = (B / A) * 0.1f / 0.1f;
			}
		}
		if(Application.loadedLevel == 1)
		{
			ToSave -= Time.deltaTime;

			if(ToSave <= 0)
			{
				ToSave = 1000f;

				SaveProgress();
			}
		}
		if(PlayingQuest)
		{
			if(CurrentQuest.QuestType == QuestType.Lvl_Distance && sc_dist.DistanceDealt >= CurrentQuest.IfDistamce)
			{
				CompleteCurrentQuest();
			}
			else if(CurrentQuest.QuestType == QuestType.Lvl_Enemies && CurEnemies >= CurrentQuest.IfEnemies)
			{
				CompleteCurrentQuest();
			}
		}
	}
	public Quest GetRandomQuest(List<Quest> CurrentQuests)
	{
		Quest Provide = CurrentQuests[Random.Range(0, CurrentQuests.Capacity)];

		while(Provide.Reserved)
		{
			Provide = CurrentQuests[Random.Range(0, CurrentQuests.Capacity)];
		}

		Provide.Reserved = true;

		return Provide;
	}
	public void LoadProgress()
	{
		Debug.LogWarning("Loading progress");

		Progress = PlayerPrefs.GetInt("StoryProgress", 0);

		Story_Provider[] Providers = FindObjectsOfType<Story_Provider>();
		foreach(Story_Provider Provider in Providers)
		{
			Provider.CurrentQuestNum = PlayerPrefs.GetInt(Provider.ID + "___Core_QNum", 0);
			
			Debug.LogWarning(Provider.name + " : " + Provider.CurrentQuestNum);
		}
	}
	public void SaveProgress()
	{
		Debug.LogWarning("Saving progress");

		PlayerPrefs.SetInt("StoryProgress", Progress);
		
		Story_Provider[] Providers = FindObjectsOfType<Story_Provider>();
		foreach(Story_Provider Provider in Providers)
		{
			PlayerPrefs.SetInt(Provider.ID + "___Core_QNum", Provider.CurrentQuestNum);
			
			Debug.LogWarning(Provider.name + " : " + Provider.CurrentQuestNum);
		}
	}
	public void SaveProgressNLoad(int ID)
	{
		PlayerPrefs.SetInt("StoryProgress", Progress);
		
		Story_Provider[] Providers = FindObjectsOfType<Story_Provider>();
		foreach(Story_Provider Provider in Providers)
		{
			PlayerPrefs.SetInt(Provider.ID + "___Core_QNum", Provider.CurrentQuestNum);
		}

		Application.LoadLevel(ID);
	}
	public void CompleteCurrentQuest()
	{
		CurrentQuest.Done = true;

		Progress = Progress + CurrentQuest.Progress;
		
		PlayerPrefs.SetInt(CurrentQuest.ProviderID + "___Core_QNum", PlayerPrefs.GetInt(CurrentQuest.ProviderID + "___Core_QNum", 0) + 1);

		SaveProgressNLoad(1);
	}
}