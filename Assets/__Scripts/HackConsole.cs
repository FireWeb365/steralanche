﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HackConsole : MonoBehaviour
{
	public InputField IF;

	void Update()
	{
		string Inp = IF.text;

		if(Inp == "RESET")
		{
			PlayerPrefs.DeleteAll();
		}
		else if(Inp == "WIN")
		{
			Application.LoadLevel("BossFight");
		}
	}
}