﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ResetHighscore : MonoBehaviour
{
	void Update()
	{
		PlayerPrefs.DeleteAll();

		DestroyImmediate(this.GetComponent<ResetHighscore>());
	}
}