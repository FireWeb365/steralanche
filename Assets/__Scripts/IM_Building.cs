﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IM_Building : MonoBehaviour
{
	public float DistanceToOpen = 4f;
	public CanvasGroup CG;
	GameObject Player;

	void Awake()
	{
		Player = GameObject.FindWithTag("Player");
	}
	void Update()
	{
		if(Vector3.Distance(Player.transform.position, transform.position) <= DistanceToOpen)
		{
			CG.alpha = Mathf.Lerp(CG.alpha, 1, Time.deltaTime * 1);
			CG.interactable = true;
			CG.blocksRaycasts = true;
		}
		else
		{
			CG.alpha = Mathf.Lerp(CG.alpha, 0, Time.deltaTime * 8f);
			CG.interactable = false;
			CG.blocksRaycasts = false;
		}
	}
}