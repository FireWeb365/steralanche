﻿using UnityEngine;
using System.Collections;

public class Player_Hooker : MonoBehaviour
{
	public int MouseBTN = 1;
	public Rigidbody2D JointPoint;
	public bool FrozeTimeOnStart = true;
	public float ReduceDistanceTime = 0.1f;
	public LayerMask HookLM;
	public GameObject LavaCollider;
	public AudioSource audio;
	Player_Rope RopeC;
	Player_Physics PlayerP;
	GameCotrol GC;
	bool DoTime;
	float TillLavaCol = 1.2f;

	void Awake()
	{
		RopeC = FindObjectOfType<Player_Rope>();
		PlayerP = FindObjectOfType<Player_Physics>();
		GC = FindObjectOfType<GameCotrol>();

		if(FrozeTimeOnStart)
		{
			Time.timeScale = 0.05f;
		}
	}
	void Update()
	{
		TillLavaCol -= Time.deltaTime;

		if(TillLavaCol <= 0f)
		{
			if(LavaCollider != null)
			{
				LavaCollider.SetActive(true);
			}
		}

		Time.fixedDeltaTime = Time.timeScale * 0.01f;

		PlayerP.Joint.distance -= Time.deltaTime * ReduceDistanceTime;

		if(DoTime && !GC.TimeStopped && !GC.Lost)
		{
			Time.timeScale = Mathf.SmoothStep(Time.timeScale, 1f, Time.deltaTime * 30);
		}

		if(Input.GetMouseButtonDown(MouseBTN) && !GC.Lost && !GC.TimeStopped)
		{
			DoTime = true;

			if(RopeC.Visible)
			{
				RopeC.Visible = false;
				
				PlayerP.Joint.enabled = false;
			}
			else
			{
				Vector2 Normal = new Vector2(-(transform.position.x - Camera.main.ScreenToWorldPoint(Input.mousePosition).x), -(transform.position.y - Camera.main.ScreenToWorldPoint(Input.mousePosition).y));
				Normal.Normalize();

				RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Normal, 16f, HookLM);

				if(hit.collider != null && hit.collider.tag != gameObject.tag && hit.collider.tag != "Killer")
				{
					if(audio != null)
					{
						audio.Play();
					}

					RopeC.Visible = true;
					
					PlayerP.Joint.enabled = true;

					JointPoint.transform.position = hit.point;

					RopeC.TargetPos = hit.point;

					RopeC.Reset = true;

					PlayerP.Joint.connectedBody = JointPoint;

					PlayerP.Joint.distance = Vector2.Distance(transform.position, new Vector2(hit.point.x, hit.point.y));
				}
			}
		}
	}
}