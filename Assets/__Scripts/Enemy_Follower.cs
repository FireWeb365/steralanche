﻿using UnityEngine;
using System.Collections;

public class Enemy_Follower : MonoBehaviour
{
	public float Speed = 3f;
	public bool AutoTargetPlayer = true;
	public GameObject TargetObject;
	Rigidbody2D RB;

	void Awake()
	{
		RB = GetComponent<Rigidbody2D>();

		if(AutoTargetPlayer)
		{
			TargetObject = FindObjectOfType<Player_Main>().gameObject;
		}
	}
	void Update()
	{
		Vector2 Normal = new Vector2(-(transform.position.x - TargetObject.transform.position.x), -(transform.position.y - TargetObject.transform.position.y));
		Normal.Normalize();

		RB.AddForce(Normal * Speed * Time.deltaTime);
	}
}