﻿using UnityEngine;
using System.Collections;

public class AnimSpeed : MonoBehaviour
{
	public string Name;
	public float Speed = 0.3f;

	void Start()
	{
		GetComponent<Animation>()[Name].speed = Speed;
	}
}